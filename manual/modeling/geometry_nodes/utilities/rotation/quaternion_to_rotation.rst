.. index:: Geometry Nodes; Quaternion to Rotation
.. _bpy.types.FunctionNodeQuaternionToRotation:

***************************
Quaternion to Rotation Node
***************************

.. figure:: /images/node-types_FunctionNodeQuaternionToRotation.png
   :align: right
   :alt: Quaternion to Rotation node.

The *Quaternion to Rotation* node converts a :ref:`quaternion rotation <quaternion mode>` to  a standard rotation.

Inputs
======

W
    The W value of the quaternion.
X
    The X value of the quaternion.
Y
    The Y value of the quaternion.
Z
    The Z value of the quaternion.


Outputs
=======

Rotation
    Standard rotation value.
